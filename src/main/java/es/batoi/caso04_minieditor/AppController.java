package es.batoi.caso04_minieditor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

	private final String ULTIMA_RUTA_PROPIEDAD = "ultimaRuta";

	@FXML
	private TextArea taEditor;
	@FXML
	private Label lblInfo;
	@FXML
	private Button btnAbrir, btnCerrar, btnGuardar, btnNuevo, btnUrl;
	@FXML
	private TextField tfEditorUrl;

	private Stage escenario;
	private File f;
	private String ultimaRuta;
	ArchivoProperties properties;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		properties = new ArchivoProperties();
		if(f==null){
			btnGuardar.setDisable(true);
		}
		taEditor.textProperty()
				.addListener((ObservableValue<? extends String> obs, String oldValor, String newValor) -> {
					if (!oldValor.equals(newValor)) {						
						int pos = 0;
						int numLin = 1;
						while (pos > -1) {
							pos = newValor.indexOf(System.getProperty("line.separator"), pos);
							if (pos > -1) {
								pos++;
								numLin++;
							}
						}
						muestraInfo(numLin, newValor.length() - numLin + 1);
					}
				});


	}

	private void muestraInfo(int numLin, int numCaracteres) {
		lblInfo.setText(numLin + " lineas - " + numCaracteres + " caracteres");
	}

	@FXML
	private void handleNuevo() {
		desactivarEditor();
		vaciarEditor();
		taEditor.setPromptText("Nuevo documento");
		System.out.println("Has pulsado botón nuevo");
		btnGuardar.setDisable(false);
	}

	private void vaciarEditor() {
		taEditor.clear();
	}

	@FXML
	private void handleAbrir() {

		if(f!=null){
			handleGuardar();
		}

		FileChooser fc = new FileChooser();
		String ultimaRuta = properties.obtenerPropiedad(ULTIMA_RUTA_PROPIEDAD);
		if(ultimaRuta!=null){
			fc.setInitialDirectory(new File(new File(ultimaRuta).getParent()));
		}
		fc.setTitle("ABRIR ARCHIVO");
		f = fc.showOpenDialog(escenario);
		if(f==null){
			taEditor.setText("No se ha seleccionado ningún archivo.");
		}else{
			System.out.println("Archivo seleccionado para abrir: " + f.getAbsolutePath());
			properties.insertarPropiedad(ULTIMA_RUTA_PROPIEDAD, f.getAbsolutePath());
			btnGuardar.setDisable(false);
			taEditor.clear();
			desactivarEditor();
			try {
				/*FileReader fr = new FileReader(f.getAbsolutePath());
				BufferedReader br = new BufferedReader(fr);
				String linea = "";

				do{
				linea = br.readLine();
					taEditor.setText(taEditor.getText()+linea+"\n");
					if(linea!=null){
						break;
					}
				}while (true);
				fr.close();
				br.close();

				*/
				List<String> texto = Files.readAllLines(Path.of(f.getAbsolutePath()));
				taEditor.setText(String.valueOf(texto));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}


	}

	private void desactivarEditor() {
		taEditor.setDisable(false);
	}

	@FXML
	private void handleGuardar() {
		FileChooser fc = new FileChooser();
		String ultimaRuta = properties.obtenerPropiedad(ULTIMA_RUTA_PROPIEDAD);
		if(ultimaRuta!=null){
			fc.setInitialDirectory(new File(new File(ultimaRuta).getParent()));
		}
		fc.setTitle("GUARDAR ARCHIVO");
		f = fc.showSaveDialog(escenario);
		if(f!=null){
			System.out.println("Archivo seleccionado para guardar: " + f.getAbsolutePath());
			properties.insertarPropiedad(ULTIMA_RUTA_PROPIEDAD, f.getAbsolutePath());
			try {
				/*FileWriter fw = new FileWriter(f.getAbsolutePath(), true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(taEditor.getText());*/
				byte[] texto = taEditor.getText().getBytes(StandardCharsets.UTF_8);
				Files.write(Path.of(f.getAbsolutePath()), texto);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("Se ha cancelado la guardación");
		}

	}

	@FXML
	private void handleCerrar() {
		handleGuardar();
		if(f!=null){
			vaciarEditor();
			desactivarEditor();
			btnGuardar.setDisable(true);
		}

	}

	@FXML
	private void handleCargarUrl(){

		if(f!=null){
			handleGuardar();
		}

		try {
			URL url = new URL(tfEditorUrl.getText());
			String linea;
			try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
				while ((linea = br.readLine()) != null) {
					taEditor.setText(taEditor.getText()+linea+System.getProperty("line.separator"));
				}
				taEditor.setDisable(false);
				btnGuardar.setDisable(false);
			} catch (IOException ex) {
				taEditor.setText("Error de lectura en origen");
			}

		} catch (MalformedURLException e) {
			taEditor.setText("URL incorrecta");
		}
	}

	void setEscenario(Stage escenario) {
		this.escenario = escenario;
	}

}
