package es.batoi.caso04_minieditor;

import java.io.*;
import java.util.Properties;

public class ArchivoProperties {

    private File f;
    FileInputStream fis;
    FileOutputStream fos;
    private Properties ps;

    public ArchivoProperties(){
        crearPropiedades();
    }

    private void crearPropiedades(){

        /*if(System.getProperty("editorpropiedades.properties")==null){
            f = new File(System.getProperty("user.home"), "editorpropiedades.properties");
            ps = new Properties();
            try {
                ps.store(new FileOutputStream(f), "Propiedades del editor");
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(System.getProperty("editorpropiedades.properties"));
        }*/

        f = new File(System.getProperty("user.home"), "editorpropiedades.properties");
        ps = new Properties();

    }

    public void insertarPropiedad(String propiedad, String valorPropiedad){
        try {
            ps.setProperty(propiedad,valorPropiedad);
            ps.store(new FileOutputStream(f), "Propiedades del editor");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String obtenerPropiedad(String propiedad){
        return ps.getProperty(propiedad);
    }
}
